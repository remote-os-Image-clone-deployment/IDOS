# Image Deployment Operating System (IDOS)
This is the operating system environment used for imaging with ROSIM. This is a linux based operating system with the tools/programs required for perform imaging tasks. This IDOS is based on Fog OS (FOS) of Fog project.

# What do we need?
1. You'll need to install docker on your system.
2. You'll need to install git on your system.
3. You'll need to clone this repository, checkout to branch IDOS.

# How do I get the IDOS repository?
Pull the IDOS repository branch with:

```
git clone https://gitlab.com/remote-os-Image-clone-deployment/IDOS.git
```

# How do I get the docker image?
Pull the idos-builder image with:
```
docker pull  registry.gitlab.com/remote-os-image-clone-deployment/idos/idos-builder:1.0
```

##### NOTES:
1. This container does not contain IDOS or the kernels, It is just a full build environment.
2. `/path/to/IDOS/repo` is not the real path, this is the path to the idos repository local to the machine you plan on running this on. Typically this would be something like `~/IDOS`. This is the only path you will need to change to build.

#### How to build?
To build the IDOS , once pulled:


```
docker run  -v /path/to/IDOS/repo:/home/builder/IDOS:Z  -it registry.gitlab.com/remote-os-image-clone-deployment/idos/idos-builder:1.0 /home/builder/IDOS/build.sh 
```
After the build, find the IDOS kernel in idos_src/output/images folder.
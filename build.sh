#!/bin/bash
BUILDROOT_VERSION="2019.05"
Usage() {
    echo -e "Usage: $0  [-b vm][-t man]"
    echo -e "\t\t-t --type [man|auto] (optional) pick the type mode of IDOS operating. Default type is man."
    echo -e "\t\t-b --build [vm|lan]  pick the system to build for. Default is to build for all."
    # echo -e "\t\t-f --filesystem-only (optional) Build the FOG filesystem but not the kernel."
    # echo -e "\t\t-k --kernel-only (optional) Build the FOG kernel but not the filesystem."
    # echo -e "\t\t-p --path (optional) Specify a path to download and build the sources."
    # echo -e "\t\t-n --noconfirm (optional) Build systems without confirmation."
    echo -e "\t\t-h --help -? Display this message."
}
dots() {
    local pad=$(printf "%0.1s" "."{1..60})
    printf " * %s%*.*s" "$1" 0 $((60-${#1})) "$pad"
    return 0
}   

brURL="https://buildroot.org/downloads/buildroot-$BUILDROOT_VERSION.tar.bz2"
#  debDeps="git meld build-essential rsync libncurses5-dev bison flex gcc-aarch64-linux-gnu"
#  rhelDeps="git meld rsync ncurses-devel bison flex gcc-aarch64-linux-gnu"
# [[ -z $arch ]] && arch="x64 x86 arm64"
# [[ -z $buildPath ]] && buildPath=$(dirname $(readlink -f $0))
# [[ -z $confirm ]] && confirm="y"

# echo "Checking packages needed for building"
# if grep -iqE "Debian" /proc/version ; then
#     os="deb"
#     eabi="eabi"
#     pkgmgr="dpkg -s"
# elif grep -iqE "Red Hat|Redhat" /proc/version ; then
#     os="rhel"
#     eabi=""
#     pkgmgr="rpm -qi"
# fi
# osDeps=${os}Deps
# for pkg in ${!osDeps}
# do
#     $pkgmgr $pkg >/dev/null 2>&1
#     if [[ $? != 0 ]]; then
#         echo " * Package $pkg missing!"
#         fail=1
#     fi
# done
# if [[ $fail == 1 ]]; then
#     echo "Package(s) missing, can't build, exiting now."
#     exit 1
# fi

# cd $buildPath || exit 1

    
    echo "Preparing buildroot  build"
    if [[ ! -d idos_src ]]; then
        if [[ ! -f buildroot-$BUILDROOT_VERSION.tar.bz2 ]]; then
            dots "Downloading buildroot source package"
            wget -q $brURL
            echo "Done"
        fi
        dots "Extracting buildroot sources"
        tar xjf buildroot-$BUILDROOT_VERSION.tar.bz2
        mv buildroot-$BUILDROOT_VERSION idos_src
        echo "Done"
    fi
    dots "Preparing code"
     cd idos_src
    if [[ ! -f .packConfDone ]]; then
        cat package/Config.in > /tmp/tmp.config
        cat ../Buildroot/package/newConf.in > package/Config.in ## adding the custom package into buildroot package configuration
        cat /tmp/tmp.config >> package/Config.in  
        touch .packConfDone
    fi

    rsync -avPrI ../Buildroot/ . > /dev/null
    # sed -i "s/^export initversion=[0-9][0-9]*$/export initversion=$(date +%Y%m%d)/" board/FOG/FOS/rootfs_overlay/bin/fog
    # if [[ ! -f .config ]]; then
    #     cp ../IDOS-configs/menuconfig.config .config
    #     case "${arch}" in
    #         x64)
    #             make oldconfig
    #             ;;
    #         x86)
    #             make ARCH=i486 oldconfig
    #             ;;
    #         arm64)
    #             make ARCH=aarch64 CROSS_COMPILE=aarch64-linux-gnu- oldconfig
    #             ;;
    #         *)
    #             make oldconfig
    #             ;;
    #     esac
    # fi
    # echo "Done"
    cp ../configs/menuconfig.config .config
    # if [[ $confirm != n ]]; then
    #     read -p "We are ready to build. Would you like to edit the config file [y|n]?" config
    #     if [[ $config == y ]]; then
    #         case "${arch}" in
    #             x64)
    #                 make menuconfig
    #                 ;;
    #             x86)
    #                 make ARCH=i486 menuconfig
    #                 ;;
    #             arm64)
    #                 make ARCH=aarch64 CROSS_COMPILE=aarch64-linux-gnu- menuconfig
    #                 ;;
    #             *)
    #                 make menuconfig
    #                 ;;
    #         esac
    #     else
    #         echo "Ok, running make oldconfig instead to ensure the config is clean."
    #         case "${arch}" in
    #             x64)
    #                 make oldconfig
    #                 ;;
    #             x86)
    #                 make ARCH=i486 oldconfig
    #                 ;;
    #             arm64)
    #                 make ARCH=aarch64 CROSS_COMPILE=aarch64-linux-gnu- oldconfig
    #                 ;;
    #             *)
    #                 make oldconfig
    #                 ;;
    #         esac
    #     fi
    #     read -p "We are ready to build are you [y|n]?" ready
    #     if [[ $ready == n ]]; then
    #         echo "Nothing to build!? Skipping."
    #         cd ..
    #         return
    #     fi
    # fi
    
    # case "${arch}" in
    #     x64)
    #         make >buildroot$arch.log 2>&1
    #         status=$?
    #         ;;
    #     x86)
    #         make ARCH=i486 >buildroot$arch.log 2>&1
    #         status=$?
    #         ;;
    #     arm64)
    #         make ARCH=aarch64 CROSS_COMPILE=aarch64-linux-gnu- >buildroot$arch.log 2>&1
    #         status=$?
    #         ;;
    #     *)
    #         make >buildroot$arch.log 2>&1
    #         status=$?
    #         ;;
    # esac
    make menuconfig
    bash -c "while true; do echo \$(date) - building ...; sleep 30s; done" &
    PING_LOOP_PID=$!
    make >buildroot.log 2>&1
    status=$?
    kill $PING_LOOP_PID
    [[ $status -gt 0 ]] && tail buildroot.log && exit $status
    # dist directory filesystem image
#     [[ ! -d dist ]] && mkdir dist 
#     case "${arch}" in
#         x*)
#             compiledfile="idos_src/output/images/rootfs.ext2.xz"
#             ;;
#         arm*)
#             compiledfile="idos_src/output/images/rootfs.cpio.gz"
#             ;;
#     esac
#     case "${arch}" in
#         x64)
#             initfile='dist/init.xz'
#             ;;
#         x86)
#             initfile='dist/init_32.xz'
#             ;;
#         arm64)
#             initfile='dist/arm_init.cpio.gz'
#             ;;
#     esac
#     [[ ! -f $compiledfile ]] && echo 'File not found.' || cp $compiledfile $initfile
# }

# function buildKernel() {
#     local arch="$1"
#     [[ -z $KERNEL_VERSION ]] && echo "No kernel version, set environment KERNEL_VERSION" && exit 1
#     echo "Preparing kernel $arch build:"
#     [[ -d kernelsource$arch ]] && rm -rf kernelsource$arch
#     if [[ ! -f linux-$KERNEL_VERSION.tar.xz ]]; then
#         dots "Downloading kernel source"
#         wget -q $kernelURL
#         echo "Done"
#     fi
#     dots "Extracting kernel source"
#     tar xJf linux-$KERNEL_VERSION.tar.xz
#     mv linux-$KERNEL_VERSION kernelsource$arch
#     echo "Done"
#     dots "Preparing kernel source"
#     cd kernelsource$arch
#     make mrproper
#     cp ../configs/.config .config
#     echo "Done"
#     if [[ -f ../patch/kernel/linux-$KERNEL_VERSION.patch ]]; then
#         dots "Applying patch(es)"
# 	echo
#         patch -p1 < ../patch/kernel/linux-$KERNEL_VERSION.patch
#         if [[ $? -ne 0 ]]; then
#             echo "Failed"
#             exit 1
# 	fi
#     else
#         echo " * Did not find a patch file matching the exact kernel version $KERNEL_VERSION."
# 	latest=$(ls -1r ../patch/kernel/linux*.patch | head -1)
# 	dots "Trying to apply $latest"
# 	echo
# 	patch -p1 < $latest
#         if [[ $? -ne 0 ]]; then
#             echo "Failed"
#             exit 1
# 	fi
#     fi
#     dots "Cloning Linux firmware repository"
#     git clone git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git >/dev/null 2>&1
#     echo "Done"
#     if [[ $confirm != n ]]; then
#         read -p "We are ready to build. Would you like to edit the config file [y|n]?" config
#         if [[ $config == y ]]; then
#             case "${arch}" in
#                 x64)
#                     make menuconfig
#                     ;;
#                 x86)
#                     make ARCH=i386 menuconfig
#                     ;;
#                 arm64)
#                     make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- menuconfig
#                     ;;
#                 *)
#                     make menuconfig
#                     ;;
#             esac
#         else
#             echo "Ok, running make oldconfig instead to ensure the config is clean."
#             case "${arch}" in
#                 x64)
#                     make oldconfig
#                     ;;
#                 x86)
#                     make ARCH=i386 oldconfig
#                     ;;
#                 arm64)
#                     make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- oldconfig
#                     ;;
#                 *)
#                     make oldconfig
#                     ;;
#             esac
#         fi
#         read -p "We are ready to build are you [y|n]?" ready
#         if [[ $ready == y ]]; then
#             echo "This make take a long time. Get some coffee, you'll be here a while!"
#             case "${arch}" in
#                 x64)
#                     make -j $(nproc) bzImage
#                     status=$?
#                     ;;
#                 x86)
#                     make ARCH=i386 -j $(nproc) bzImage
#                     status=$?
#                     ;;
#                 arm64)
#                     make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- -j $(nproc) Image
#                     status=$?
#                     ;;
#                 *)
#                     make -j $(nproc) bzImage
#                     status=$?
#                     ;;
#             esac
#         else
#             echo "Nothing to build!? Skipping."
#             cd ..
#             return
#         fi
#         [[ $status -gt 0 ]] && exit $status
#     else
#         case "${arch}" in
#             x64)
#                 make oldconfig
#                 make -j $(nproc) bzImage
#                 status=$?
#                 ;;
#             x86)
#                 make ARCH=i386 oldconfig
#                 make ARCH=i386 -j $(nproc) bzImage
#                 status=$?
#                 ;;
#             arm64)
#                 make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- oldconfig
#                 make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- -j $(nproc) Image
#                 status=$?
#                 ;;
#             *)
#                 make oldconfig
#                 make -j $(nproc) bzImage
#                 status=$?
#                 ;;
#         esac
#     fi
#     [[ $status -gt 0 ]] && exit $status
#     cd ..
#     mkdir -p dist
#     case "$arch" in
#         x64)
#             compiledfile="kernelsource$arch/arch/x86/boot/bzImage"
#             cp $compiledfile dist/bzImage
#             ;;
#         x86)
#             compiledfile="kernelsource$arch/arch/x86/boot/bzImage"
#             cp $compiledfile dist/bzImage32
#             ;;
#         arm64)
#             compiledfile="kernelsource$arch/arch/$arch/boot/Image"
#             cp $compiledfile dist/arm_Image
#             ;;
#     esac
# }





# for buildArch in $arch
# do
#     if [[ -z $buildKernelOnly ]]; then
#         buildFilesystem $buildArch
#     fi
#     if [[ -z $buildFSOnly ]]; then
#         buildKernel $buildArch
#     fi
# done

#!/bin/bash


#ideploy.sh 
#Image deployment script, defines variables. calls other utility functional scripts needed for deployment which are :
#partition.sh -- to check, or/and create , and mount hidden partition
#menu.sh - GUI to display and selection of avilable images on NFS-server
#deploying.sh - Deploys the selected image
#Finally, this script writes signature at the end of disk ( starting from 200 bytes from the end ) to signal 
#network grub to boot from local hdd rather than from the tiny operating system image present on tftp server


mounting_point_remote="/root/" # mounting point of NFS server in IDOS
mounting_point_hidden="/mnt"  # mount point of hidden partition 
DATE=`date`
log_file="$mounting_point_remote/log/$MAC.log" #log_file address on NFS-server
signature="hepia2015"       #Signature as signal  which is written to end of disk  after image deployment
images_dir="$mounting_point_remote/images"
declare  -A partitions
((part_no=0))
boundary="######################################$DATE##############################################"
echo  -e "\n$boundary\n" && echo -e "Imaging\n"
((min_start=1048576))
((size_num= `fdisk -l /dev/sda| grep "Disk /dev/sda:" | cut -d ',' -f 2 | cut -d ' ' -f 2`  )) && echo "Total size of disk=$size_num bytes"   # size in bytes
((h_size_max_part= 20 * size_num / 100)) && echo "size of max_hidden partition=$h_size_max_part bytes" 
IMAGE_CHOICE=''

if [[ -z $1 ]]; then
. $IDOS_scripts/menu.sh
else
 IMAGE_CHOICE="$1"
 echo "image choice is $IMAGE_CHOICE"
fi

. $IDOS_scripts/deploying.sh



((size_num_sign=size_num - 200)) #Starting location of signature 

# echo "location of signature at  $size_num_sign B" && echo " signature is $signature"
# echo -ne "$signature" | dd of=/dev/sda seek=$size_num_sign bs=1 iflag=skip_bytes  

 
#clear
echo "rebooting.." 
DATE=`date`
boundary="######################################$DATE##############################################"
echo  -e "\n$boundary\n" 
 cd /

exit 0

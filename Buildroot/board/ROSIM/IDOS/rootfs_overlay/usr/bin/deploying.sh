#!/bin/bash


#deploying.sh 
#Checks for cache i.e. in the hidden partition of harddisk, for  selected image if present, and deploys from hidden partition.
#checking is done by comparing md5sum of files present localy and on remote nfs server.
# If image cache not present, downloads the image from nfs server onto hidden partition if space is available, and then deploys the image.
#If no space available in hidden partition, it just deploys from nfs server without making a copy locally.
#Some Cache replacement strategies need to be applied to overcome no-space for cache available problem 

((hidden_part_mount=0))
((hidden_part_flag=0))
create_partition_x=''
function create_partition_msdos () {
	echo "creating partition in msods table"  
	((no_primary_p=`parted -l | grep -c "primary" `))
	echo "start_p is $start_p"
	echo "end_p is $end_p"
	((start_p_s=start_p/512))
	((end_p_s=end_p/512 ))
	((fstart_s=fstart/512))
	((fend_s=fend/512))
	if [[ $no_primary_p -lt 3 ]]; then
					echo "primary partition"
					parted -s  /dev/sda mkpart primary $start_p_s"s"  $end_p_s"s"

	else
		((extended_p=`parted -l | grep -c "extended"`))
		if (( extended_p ));then
			echo "logical"
			parted -s  /dev/sda mkpart logical $start_p_s"s"  $end_p_s"s"
		else 
			parted -s  /dev/sda mkpart extended $fstart_s"s"  $fend_s"s"
			((start_p=start_p+min_start))
			((end_p=end_p+min_start)) && echo "end is $end_p'"
			((start_p_s=start_p/512))
			((end_p_s=end_p/512 ))
			parted -s  /dev/sda mkpart logical $start_p_s"s"  $end_p_s"s" && echo "start is $start_p_s"
		fi
	fi	 

	partprobe
	# key="$3"
	local part_no=`parted /dev/sda 'unit s print' | grep -A 50 "Start" | tail +2 |awk '{print $1,$2}' | grep $start_p_s | awk '{print $1}'` 
	partitions[$key]="/dev/sda$part_no" && echo "$key is /dev/sda$part_no"
}

function create_partition_gpt () {
	parted -s  /dev/sda mkpart  $start_p"B"  $end_p"B"
	partprobe
	((fstart=end_p))
    local part_no=`parted /dev/sda 'unit B print' | grep -A 50 "Start" | tail +2 |awk '{print $1,$2}' | grep $start_p | awk '{print $1}'` 
	partitions[$key]="/dev/sda$part_no" 
}

# assumes contiguos enough  one free space available for imaging
# should there be gap between th partitions? not aligned error? Windows won't work
function partitioning() {
	free_part=''
	((partition_flag=0))
	((no_of_free_space=` parted -s /dev/sda  'unit  B print' print free | grep "Free" | wc -l`))
	#find that contiguous block that can fit all the to be deployed partitions
	for ((j=1;j<=no_of_free_space;j++)); do
		free_part=`parted -s /dev/sda  'unit  B print' print free | grep "Free" | tail +$j | head -n 1`
		((fsize= `echo $free_part | awk '{print $3}' | cut -d 'B' -f 1`))
		echo "hello"
		
		if [[ $fsize -ge $size_disk ]];then
			((partition_flag=1)) && echo "got a free space" && sleep 3
			break
		fi
	done

	if [[ $partition_flag -eq 0 ]]; then #no contiguous space found
	  echo "no enough free space found"
	 exit 1
	fi
	
	((fsize= `echo $free_part | awk '{print $3}' | cut -d 'B' -f 1`))
	((fstart= `echo $free_part | awk '{print $1}' | cut -d 'B' -f 1`))
	((fend= `echo $free_part | awk '{print $2}' | cut -d 'B' -f 1`))
	if [[ $fstart -lt  $min_start ]] ; then
		((fstart=min_start))
	fi

	#allocating the partitions for deployment
	for ((i=1; i<=total_partition ; i++)) ; do
	temp_line=`cat $images_dir/$IMAGE_CHOICE/partition | grep -A 50 "Start" | tail +2 | tail +$i | head -n 1` && echo " temp is $temp_line" && sleep 2
	((current_partition= `echo $temp_line  | awk '{ print $1}'` )) && echo "$current_partition"
     ptype=`echo $temp_line | awk '{ print $5}' ` && echo "ptype is $ptype"
	 if [[ $ptype == "extended" ]]; then
          echo "extended"
          continue
  	fi
	if [[ $SWAP_PRESENT -eq 1 ]] &&  [[ "$swap_partition" == "/dev/sda$current_partition" ]];then
	 key="swap" && echo "$key"
	else
	  key="p$current_partition"  && echo "$key"
	fi
	((size_p=`echo $temp_line | awk '{print $4}' | cut -d "B" -f 1`)) && echo " size of part is $size_p"
	((start_p=fstart)) 	
	((end_p=start_p+size_p-1)) && echo "end is $end_p'"
	$create_partition_x	# create_partition_msdos #creates partition
	((fstart=end_p+1))
	done
 
	return 0
}
function hidden_partition_check(){
	#checking if hidden present already or not
	if [[  -e $mounting_point_remote/hidden/$MAC.p ]] ; then
			end_p=`cat $mounting_point_remote/hidden/$MAC.p | head -n 1 | awk '{print $3}'`
			part_no=`parted /dev/sda 'unit S print' | grep -A 50 "Start" | tail +2 |awk '{print $1,$3}' | grep $end_p | awk '{print $1}'` 
			partitions["hidden"]="/dev/sda$part_no" && echo " hidden partition is ${partitions["hidden"]}" 
			sleep 5
			hidden_part_flag=1
	return 0
	fi
	
	return 1
}
function hidden_partition (){
	local part_no="0"
	#check if enough space available and creating hidden partition
	((no_of_free_space= `parted -s /dev/sda  'unit  B print' print free | grep "Free" | wc -l`)) && echo "free space $no_of_free_space"
	hidden_part=`parted -s /dev/sda  'unit  B print' print free | grep "Free" | tail +$no_of_free_space | head -n 1` && echo "hidden partition is $hidden_part"
	((hidden_size= `echo $hidden_part | awk '{print $3}' | cut -d 'B' -f 1`))
	((fstart= `echo $hidden_part | awk '{print $1}' | cut -d 'B' -f 1`))
	if [[ $fstart -lt  $min_start ]] ; then
		((fstart=min_start))
	fi
    
	((fend= `echo $hidden_part | awk '{print $2}' | cut -d 'B' -f 1`))
	if [[ $hidden_size -ge $h_size_min_part ]]; then
	if [[ $hidden_size -ge $h_size_max_part ]]; then
	((end_p=`echo $hidden_part | awk '{print $2}' | cut -d 'B' -f 1`))  && echo "end is $end_p "
	((start_p=end_p-h_size_max_part)) && echo "start is $start_p"
	else
		((end_p=`echo $hidden_part | awk '{print $2}' | cut -d 'B' -f 1`)) 
		((start_p=`echo $hidden_part | awk '{print $1}' | cut -d 'B' -f 1`))
	fi
	((end_p=end_p-2*min_start))
	key="hidden"
	$create_partition_x	# create_partition whose table can be msdos or gpt
	echo "$key Partition is ${partitions[$key]}"
	mke2fs -t ext4 ${partitions["hidden"]}  && echo "hidden partition created and available" && parted /dev/sda 'unit S print' | grep -A 50 "Start" | tail +2 | grep $start_p_s   >  $mounting_point_remote/hidden/$MAC.p   #formating into ext4 fs

	fi 

	umount $mounting_point_hidden 2>/dev/null
	mount ${partitions["hidden"]} $mounting_point_hidden && echo "mounted" && ((hidden_part_mount=1))  #mounting the hidden partition
}

#identify properly if EFI partition present, after EFI partition place appropriate files in EFI partition
# msdos_table partition OS image to GPT partition? Linux might work ?

#function to check if the md5sum of the specified image in remote and those present lcoally match

function check_md5sum_deploy () {
	
	for i in `ls -F $1/ | grep \/$` ; do
                  echo $i
		local_hash=`head -n 1 $mounting_point_hidden/$i/md5 | cut -d " " -f 1` 
		if [[ $local_hash == $hash ]]; then
		for j in `ls $mounting_point_hidden/$i/*.img` ; do
 	    	 temp=`echo $j | grep -o p[0-9]*.img | cut -d "." -f 1`  && echo "partition is $temp"
 	   
			cat $j | gunzip -c |  partclone.restore -N -d -s - -o ${partitions[$temp]} 
        done	
		echo "cached image deployed"
        ((cache_flag=ALREADY_CACHED))
		return $cache_flag
		fi
	done
	echo "not cached, so caching "  
	size_cache_available=`df $mounting_point_hidden/ | awk '{print $4}' | tail -n 1 ` && echo "size cache available=$size_cache_available B"   #in KB
	((size_cache_available=size_cache_available*1024))
	((size_cache_available=size_cache_available-4000))
	size_cache_img=`cat $images_dir/$IMAGE_CHOICE/size |tail +2| head -n 1 | awk '{print $1}'` && echo "size of image to download=$size_cache_img B"    #in KB
	   
	#Check enough space available in hidden partition for caching
	if [[ $size_cache_img -lt $size_cache_available ]]; then
	((cache_flag=CACHE_NOW))
	else
	echo  "not enough space for caching"  #further scope for some cache replacement strategy
	((cache_flag=CANT_CACHE))
	fi


	return $cache_flag

}

((ALREADY_CACHED=0))
((CACHE_NOW=1))
((CANT_CACHE=-1))


size_disk=`cat $images_dir/$IMAGE_CHOICE/size |tail +1| head -n 1 | awk '{print $1}'` && echo "total_size of image=$size_disk B"
total_partition=`cat $images_dir/$IMAGE_CHOICE/partition | grep "total_partition"| cut -d '=' -f 2` && echo "total partitions are $total_partition"
((SWAP_PRESENT=0))
swap_partition=`cat  $images_dir/$IMAGE_CHOICE/UUIDS | grep "swap" | cut -d ":" -f 1`
if [[ -n $swap_partition ]]; then # check if swap partition present
((SWAP_PRESENT=1))
swap_UUID=`cat  $images_dir/$IMAGE_CHOICE/UUIDS | grep "swap" | cut -d "=" -f 2 | cut -d '"' -f 2`
fi

partition_t=`parted -l | grep "Partition Table" | cut -d ':' -f 2 | cut -d ' ' -f 2` && echo "partition table $partition_t"

((min_start=1048576))


# two options : image overwrite the existing partitions or image to not disturb the existing OSes
#image overwrite if erase_fresh=1, shouldn't erase the hidden partition, efi partition
if [[ $partition_t == "msdos" ]]; then
	echo "msdos"
	create_partition_x="create_partition_msdos"
  	# partitioning && echo "msdos"
elif [[ $partition_t == "gpt" ]]; then
	create_partition_x="create_partition_gpt"
	# gpt_table && echo "gpt"
else 
	echo 'bye'
	exit 0
fi
hidden_partition_check
if [[ $erase_fresh -eq 1 ]]; then
declare -a partition_erase
	total_partition_erase=`fdisk -l /dev/sda | grep -A 50 "Start" | tail +2 | grep -c "/dev/s"`
	for ((i=1;i<=total_partition_erase; i++)); do
	temp_line=`parted /dev/sda 'unit B print' | grep -A 50 "Start" | tail +2 |  tail +$i | head -n 1` && echo "$temp_line"
	((part_no=`echo $temp_line | awk '{ print $1}'`)) && echo " partition is /dev/sda$part_no"	
	if [[ $hidden_part_flag -eq 1 ]] && [[ ${partitions["hidden"]} == "/dev/sda$part_no" ]]; then
	continue
	fi
    ((partition_erase[$i]=part_no))
	done
	for part_no in ${partition_erase[@]}; do
	parted -s /dev/sda rm $part_no 
	done

fi


partitioning && echo "$create_partition_x"

if [[ $hidden_part_flag -eq 1 ]]; then
	umount $mounting_point_hidden 2>/dev/null
	mount ${partitions["hidden"]} $mounting_point_hidden  && echo "mounted" && ((hidden_part_mount=1))  #mounting the hidden partition
else
	hidden_partition
fi

echo "partitions are ${partitions[@]}, keys are ${!partitions[@]}, elements ${#partitions[@]}"
sleep 5
echo "$images_dir"
hash=`head -n 1 $images_dir/$IMAGE_CHOICE/md5 | cut -d " " -f 1` 
# exit 0
start_time=`date +%s` && \
# echo "Do you want to cache?"
# read cache_flag && echo "os name is $cache_flag"
# check_md5sum_deploy  "$mounting_point_hidden"  #Check if image to be deployed is cached  

 ((cache_flag=CANT_CACHE))

if [[ $cache_flag == $CACHE_NOW ]]; then
		
                if [ -d $mounting_point_hidden/$IMAGE_CHOICE ] ; then
                   echo "directory exists"
                else
                mkdir $mounting_point_hidden/$IMAGE_CHOICE 
                fi && \
		
				#Simultaneously make local copy and deploy 
                for i in `ls $images_dir/$IMAGE_CHOICE/*.img` ; do
                #create partitions using parted and map -- can't just flash the table as it conflicts with hidden partition - first partitioning and finally imaging the mbr
 	    	temp=`echo $i | grep -o p[0-9]*.img | cut -d "." -f 1` && echo "$temp" 
	   		cat  $i | tee $mounting_point_hidden/$IMAGE_CHOICE/"$temp.img" | gunzip -c |  partclone.restore -N -d -s - -o ${partitions[$temp]} 
           	done  && \
 	        cp $images_dir/$IMAGE_CHOICE/md5  $mounting_point_hidden/$IMAGE_CHOICE/ && \
	 	
	    
	    echo "Caching and deployment  of $size_cache_img B done in $cache_time s "	 

		if [[ $fstart -lt  $min_start ]] ; then
		((fstart=min_start))
		fi
    
elif [[ $cache_flag == $CANT_CACHE ]]; then #deploy from NFS server without making local copy
 	  for i in `ls $images_dir/$IMAGE_CHOICE/*.img` ; do
 	   temp=`echo $i | grep -o p[0-9]*.img | cut -d "." -f 1`  && echo "partition $temp" 
		if [[ $MULTICAST -eq 1 ]]; then
		udp-receiver --portbase $port_base --pip "gunzip -c" --no-progress 2>/dev/null | partclone.restore -N -d -s - -o ${partitions[$temp]} 
		else
	   	cat  $i| gunzip -c |  partclone.restore -N -d -s - -o ${partitions[$temp]} 
		fi
     done 
        
fi
umount $mounting_point_hidden
# dd if=$images_dir/$IMAGE_CHOICE/"mbr"  of=/dev/sda && \
# partprobe /dev/sda && echo "mbr imaged"   

if [[ $SWAP_PRESENT -eq 1 ]]; then # check if swap partition present
mkswap -U $swap_UUID ${partitions["swap"]}  # create swap partition with same swap UUID
fi

grub-mkconfig -o  /tmp/grub.cfg && echo "grub.cfg done"
configuration_grub=`echo $MAC | tr ":" "-" `
tftp $TFTP_SERVER -c put /tmp/grub.cfg /boot/grub/grub_configs/$configuration_grub
end_time=`date +%s`
((deploy_time= end_time - start_time))

echo "time to deploy $size_disk B is $deploy_time s"  


#sleep 3

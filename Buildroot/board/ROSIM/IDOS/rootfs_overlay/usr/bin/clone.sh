#!/bin/bash
umask 000
# automate the cloning process, give option to choose the partitions for cloning
mounting_point_remote="/root/" # mounting point of NFS server in IDOS
mounting_point_hidden="/mnt"  # mount point of hidden partition 
declare  -a partitions_clone # partitions to clone **
declare -a partition_nos
((len=$2))
JSON="$3" && echo "JSON is $JSON" && sleep 4
for ((i=0;i<len;i++))  
do 
partition_nos[$i]=`echo $JSON | jq .partitionNumber[$i]  |  cut -d '"' -f 2`
done
DATE=`date`
Interface="eth0" 
MAC=`cat /sys/class/net/$Interface/address`  
log_file="$mounting_point_remote/log/$MAC.log" #log_file address on NFS-server
images_dir="$mounting_point_remote/images"
image_name="$1" #image name to be supplied**
((min_start=1048576))
boundary="######################################$DATE##############################################"
echo  -e "\n$boundary\n" && echo -e "CLONING\n"


echo "Name of the cloned OS is $image_name"
# read image_name && echo "os name is $image_name"
mkdir $images_dir/$image_name
total_partition=`fdisk -l /dev/sda | grep -A 50 "Start" | tail +2 | grep -c "/dev/s"`

echo "total Partitions= $total_partition"

if [[ $2 -eq 0 ]]; then # Assumption that the whole disk is cloned, if no partitions specified
for ((i=1;i<=total_partition; i++)); do
temp_line=`parted /dev/sda 'unit B print' | grep -A 50 "Start" | tail +2 |  tail +$i | head -n 1` && echo "$temp_line"
((part_no= `echo $temp_line | awk '{ print $1}'`)) && echo " partition is /dev/sda$part_no"
((partitions_clone[$i]=part_no))
done
echo "total_partition_cloned=${#partitions_clone[@]}" > $images_dir/$image_name/partition
else
partitions_clone=("${partition_nos[@]}")
echo "total_partition_cloned=${#particreateimagetion_nos[@]}" > $images_dir/$image_name/partition
fi

# parted /dev/sda 'unit B print' >> $images_dir/$image_name/partition

# dd if=/dev/sda bs=512 count=2048 of=$images_dir/$image_name/"mbr"
((total_size=0))
((total_image_size=0 ))
total_hash_sum=''
((j=0))
echo "partitions are ${partitions_clone[@]}"
temp_line=`parted /dev/sda 'unit B print'  | grep -A 50 "Start" | tail +1 |  head -n 1`
echo "$temp_line" >> $images_dir/$image_name/partition
for part_no in ${partitions_clone[@]} ; do
  
   temp_line_no=`parted /dev/sda 'unit B print'  | grep -A 50 "Start" | tail +2 | awk '{print $1}' | grep -n $part_no | cut -d ":" -f 1`
   temp_line=`parted /dev/sda 'unit B print'  | grep -A 50 "Start" | tail +2 | tail +$temp_line_no | head -n 1 `
   ptype=`echo $temp_line | awk '{ print $5}' ` && echo "ptype is $ptype"
   echo "$temp_line" >> $images_dir/$image_name/partition
  if [[ $ptype == "extended" ]]; then
          echo "extended"
          continue
  fi
  fs_type=`echo $temp_line | awk '{ print $6 }'` && echo "file system is $fs_type"
  ((size=`echo $temp_line | awk '{ print $4 }' | cut -d "B" -f 1`)) && echo "size of partition is $size"
  ((total_size=total_size+size))
  case "$fs_type" in 
	       "ext2") partclone.ext2 -N -c -d -s  /dev/sda$part_no | gzip -c > $images_dir/$image_name/"p$part_no.img";;
         "ext3") partclone.ext3 -N -c -d -s  /dev/sda$part_no | gzip -c > $images_dir/$image_name/"p$part_no.img" ;;
          "ext4") partclone.ext4 -N -c -d -s  /dev/sda$part_no | gzip -c > $images_dir/$image_name/"p$part_no.img" ;; 
          "ntfs") partclone.ntfs -N -c -d -s  /dev/sda$part_no | gzip -c > $images_dir/$image_name/"p$part_no.img" ;;
          "fat32") ((part_start=`echo $temp_line | awk '{print $2}' | cut -d 'B' -f 1`))
          if [[ $min_start -ne $part_start ]]; then # don't clone if efi partition
           partclone.fat32 -N -c -d -s  /dev/sda$part_no | gzip -c > $images_dir/$image_name/"p$part_no.img"
           fi
           ;;
           
           "exfat") partclone.exfat -N -c -d -s  /dev/sda$part_no | gzip -c > $images_dir/$image_name/"p$part_no.img" ;;
            *) 
         swap=`echo $fs_type | grep -o "swap"`
         if [[ -n $swap ]]; then 
          echo " /dev/sda$part_no is swap partition"
          else
              dd if=/dev/sda$part_no count=4096 | gzip -c > $images_dir/$image_name/"p$part_no.img"
          fi 
           ;;
          esac
      	  if [[ -f  $images_dir/$image_name/"p$part_no.img" ]]; then
            mount "/dev/sda$part_no" /mnt 
            if [[ -f /mnt/etc/fstab ]]; then
              cp /mnt/etc/fstab $images_dir/$image_name/.fstab
            fi
            umount /mnt
            hash_sum=`md5sum $images_dir/$image_name/"p$part_no.img" | awk '{print $1}'`
            ((j=j+1))
            if [[ $j -eq 1 ]]; then
                total_hash_sum="$hash_sum"
            else
                total_hash_sum=`echo $total_hash_sum$hash_sum | md5sum - | awk '{print   $1}'`
            fi
         
	         image_size=`du $images_dir/$image_name/"p$part_no.img" | awk '{print $1}'` && echo "$image_size"
          ((total_image_size=total_image_size+image_size )) && echo "$total_image_size"
        # sleep 15
        fi
  done
((total_image_size=total_image_size*1024)) # convertin KiB to Bytes
echo "$total_size  total_size in B" > $images_dir/$image_name/size
echo "$total_image_size  total_image size in B" >> $images_dir/$image_name/size
echo "$total_hash_sum  $image_name" > $images_dir/$image_name/md5
#blkid > $images_dir/$image_name/UUIDS
if [[ -f $images_dir/$image_name/.fstab ]]; then
for part_no in ${partitions_clone[@]} ; do
 temp_line_no=`parted /dev/sda 'unit B print'  | grep -A 50 "Start" | tail +2 | awk '{print $1}' | grep -n $part_no | cut -d ":" -f 1`
 temp_line=`parted /dev/sda 'unit B print' | grep -A 50 "Start" | tail +2 | tail +$temp_line_no | head -n 1` 
  ptype=`echo $temp_line | awk '{ print $5}'` && echo "ptype is $ptype"
# ((part_no= `echo $temp_line | awk '{ print $1}'`)) && echo " partition is /dev/sda$part_no"
  UUID_no=`blkid | grep "/dev/sda$part_no" | cut -d '=' -f 2 | cut -d '"' -f 2`
  fs_type=`echo $temp_line | awk '{ print $6 }'` && echo "file system is $fs_type"
  mounted=`cat $images_dir/$image_name/.fstab | grep $UUID_no| awk '{print $2}'`  2>/dev/null
  echo "mounted $mounted"
  line="/dev/sda$part_no: UUID=\"$UUID_no\" mounting_point=$mounted TYPE=$fs_type"
  echo  $line >> $images_dir/$image_name/UUIDS
done
fi
#rm $images_dir/$image_name/.fstab
boundary="######################################$DATE##############################################"
echo  -e "\n$boundary\n" 

#api to send info of image cloned to the database
#image_name
#os_type - Linux, windows, BSD
#partition type
#Size of image
#total size to be imaged to

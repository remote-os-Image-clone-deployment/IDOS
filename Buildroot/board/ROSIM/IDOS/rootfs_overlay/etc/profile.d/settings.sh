#!/bin/bash
settings=`curl -s $WEB_SERVER/tasks/getsettings`
web_server_ip=`echo $settings | jq .web_server_ip | cut -d '"' -f 2`
web_server_port=`echo $settings | jq .web_server_port | cut -d '"' -f 2`
export WEB_SERVER="$web_server_ip:$web_server_port"
storage_server_ip=`echo $settings | jq .storage_server_ip | cut -d '"' -f 2`
storage_server_share=`echo $settings | jq .storage_server_share | cut -d '"' -f 2`
export STORAGE_SERVER="$storage_server_ip:$storage_server_share"
export DEBUG=`echo $settings | jq .debug`
export TFTP_SERVER=`echo $settings | jq .tftp_server | cut -d '"' -f 2`
export DHCP_SERVER=`echo $settings | jq .dhcp_server | cut -d '"' -f 2`
export GLOBAL_DEPLOYMENT_MODE=`echo $settings | jq .global_deployment_mode | cut -d '"' -f 2`